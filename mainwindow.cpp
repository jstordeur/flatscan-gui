#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "flatscan/const.hpp"
#include <string>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->comboBox_portcom->addItems({"16", "17", "18", "19"});
    ui->comboBox_baudrate->addItems({"921600"});
    ui->comboBox_lineStyle->addItem("None");
    ui->comboBox_scatterStyle->addItem("circle");

    QObject::connect(ui->pushButton_connect, SIGNAL(clicked()), this, SLOT(connect()));
    QObject::connect(ui->pushButton_updateSettings, SIGNAL(clicked()), this, SLOT(update()));
    QObject::connect(ui->pushButton_plot, SIGNAL(clicked()), this, SLOT(flag()));
        timer = new QTimer(this);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));

    ui->doubleSpinBox_xAxisFrom->setValue(-3000.0);
    ui->doubleSpinBox_xAxisTo->setValue(3000.0);
    ui->doubleSpinBox_yAxisFrom->setValue(0.0);
    ui->doubleSpinBox_yAxisTo->setValue(6000);
    ui->spinBox_millis->setValue(250);

    ui->customPlot->addGraph();

    update();
}

void MainWindow::flag()
{
    if(canUpdate){
        canUpdate = false;
        ui->pushButton_plot->setText("Start logging");
    }
    else{
        canUpdate = true;
        ui->pushButton_plot->setText("Stop logging");
        printf("Clear\n");
    }
}

void MainWindow::connect()
{
    if(!connected){
        fs->connect();
        fs->init();
        ui->pushButton_plot->setEnabled(true);
        ui->pushButton_connect->setText("Disconnect");
        connected = true;
    }
    else
    {
        canUpdate = false;
        ui->pushButton_plot->setEnabled(false);
        ui->pushButton_connect->setText("Connect");
        ui->pushButton_plot->setText("Start logging");
        fs->close();
        connected = false;
    }
}

void MainWindow::update()
{
    int millis = ui->spinBox_millis->value();
    double xFrom = ui->doubleSpinBox_xAxisFrom->value();
    double xTo = ui->doubleSpinBox_xAxisTo->value();
    double yFrom = ui->doubleSpinBox_yAxisFrom->value();
    double yTo = ui->doubleSpinBox_yAxisTo->value();


    ui->customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsNone);
    ui->customPlot->graph(0)->setScatterStyle(QCPScatterStyle::ssCircle);
    ui->customPlot->xAxis->setLabel("x");
    ui->customPlot->yAxis->setLabel("y");
    ui->customPlot->xAxis->setRange(xFrom, xTo);
    ui->customPlot->yAxis->setRange(yFrom, yTo);
    ui->customPlot->replot();

    timer->start(millis); //time specified in ms
}

void MainWindow::refresh()
{
    if(canUpdate){
        std::vector<double> x;
        std::vector<double> y;

        std::vector<Rect2> pt;

        for(int j = 1; j <= 1; j++)
        {
            pt.clear();

            fs->get_MeasurmentsRect(&pt);

            for(int i = 0; i < pt.size(); i++){
                x.push_back(pt[i].x);
                y.push_back(pt[i].y);
            }
        }

        QVector<double> qx = QVector<double>::fromStdVector(x);
        QVector<double> qy = QVector<double>::fromStdVector(y);

        ui->customPlot->graph(0)->setData(qx, qy);
        ui->customPlot->replot();
    }
}

MainWindow::~MainWindow()
{
    delete fs;
    delete timer;
    delete ui;
}
