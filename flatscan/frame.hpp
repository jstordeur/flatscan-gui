#ifndef FRAME_H
#define FRAME_H

#include <stdio.h>
#include <cstring>
#include <vector>

#include "const.hpp"

class Flatscan;

class Frame {
public:
	std::vector<octet> sync; //[11]
	std::vector<octet> cmd; //[2]
	std::vector<octet> data; //[1609]
	unsigned short size = 0; //Size of data
	std::vector<octet> chk; //[2]

public:
	Frame();
	//~Frame();

	// Compute sync, cmd, data, chk into raw
	std::vector<octet> compute();
	// Convert raw into sync, cmd, data, chk
	//It seems pretty useless... Damn you're so stupid !
	void extract();

	//Well, to be deleted or not ?
	void set_Cmd(word command);

	void computeSync();
	void computeCHK();
	void set_Frame(octet* tab, int size);
	static word CRC16(octet* data, int size);
	void toString();

	friend class Flatscan;
	friend class Command;
};

#endif
