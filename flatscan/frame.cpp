#include "frame.hpp"

using namespace std;

Frame::Frame() {
	//"malloc" like
	sync.reserve(SIZE_SYNC);
	cmd.reserve(SIZE_CMD);
	data.reserve(SIZE_DATA);
	chk.reserve(SIZE_CHK);

	//HARDCODED
	//sync patern
	//sync.push_back(0x34); sync.push_back(0x12); sync.push_back(0xA0); sync.push_back(0xBE);
	sync.push_back(0xBE); sync.push_back(0xA0); sync.push_back(0x12); sync.push_back(0x34);
	//Version protocole
	sync.push_back(0x02);
	//frame length to be determined later
	sync.push_back(0x00); sync.push_back(0x00);
	//using CRC16
	sync.push_back(0x02);
	//Futur use
	sync.push_back(0x00); sync.push_back(0x00); sync.push_back(0x00);
}

vector<octet> Frame::compute() {
	vector<octet> temp;

	//concat vectors
	temp.insert(temp.end(), sync.begin(), sync.end());
	temp.insert(temp.end(), cmd.begin(), cmd.end());
	temp.insert(temp.end(), data.begin(), data.end());
	temp.insert(temp.end(), chk.begin(), chk.end());

	return temp;
}

void Frame::extract() {
	//Wow, it's empty here :o
}

//You're so lazy !
void Frame::set_Cmd(word command) {
	cmd[0] = command % 256;
	cmd[1] = command / 256;

	printf("%d %d\n", this->cmd[0], this->cmd[1]);
}

void Frame::computeSync() {
	size = SIZE_FRAME_IF_DATA_NULL + data.size();
	sync[5] = size % 256;
	sync[6] = size / 256;
}

void Frame::computeCHK() {
	//Improve compute time...
	vector<octet> toBeCompute;
	toBeCompute.insert(toBeCompute.end(), sync.begin(), sync.end());
	toBeCompute.insert(toBeCompute.end(), cmd.begin(), cmd.end());
	toBeCompute.insert(toBeCompute.end(), data.begin(), data.end());

	word crc16 = CRC16(toBeCompute.data(), toBeCompute.size());

	chk.clear();
	chk.push_back(crc16 % 256);
	chk.push_back(crc16 / 256);

}

//Wait, what I'm doing here ? :'(
word Frame::CRC16(octet* data, int size) {
	word crc = 0; /* CRC value is 16bit */
	word i, j;
	for(i = 0; i < size; i++)
	{
		crc ^= (word)(data[i] << 8); /* move OCTET into MSB of 16bit CRC */
		for (j = 0; j < 8; j++)
		{
			if ((crc & 0x8000) != 0) /* test for MSB = bit 15 */
			{
				crc = (word)((crc << 1) ^ BEA_POLYNOM);
			}
			else
			{
				crc <<= 1;
			}
		}
  }
  return crc;
}

void Frame::set_Frame(octet* tab, int size) {
	int sizeData = size - SIZE_FRAME_IF_DATA_NULL;

	sync.clear();
	cmd.clear();
	data.clear();
	chk.clear();

	if(MASTER_DEBUG && DEBUG_FRAME){
		printf("\n%s : Setting Frame\n", PREFIX_FRAME);
		printf("\tsync : from %d to %d\n", 0, SIZE_SYNC);
		printf("\tcmd : from %d to %d\n", SIZE_SYNC, SIZE_SYNC+SIZE_CMD);
		printf("\tdata : from %d to %d\n", SIZE_SYNC+SIZE_CMD, (SIZE_SYNC+SIZE_CMD) + sizeData);
		printf("\tchk : from %d to %d\n", size-SIZE_CHK, size);}

	//Improve overload for only rewrite data and chk
	sync.assign(tab, tab+SIZE_SYNC);
	cmd.assign(tab+SIZE_SYNC, tab+SIZE_SYNC+SIZE_CMD);
	if(sizeData > 0)data.assign(tab+SIZE_SYNC+SIZE_CMD, tab + (SIZE_SYNC+SIZE_CMD) + sizeData);
	chk.assign(tab+size-SIZE_CHK, tab+size);
}

void Frame::toString() {
	//Isn't it beautiful ?
	printf("-----Printing frame-----\n");

	printf("\tSYNC :\n");
	printDecToHex(sync.data(), sync.size());

	printf("\tCMD :\n");
	printDecToHex(cmd.data(), cmd.size());

	printf("\tDATA :\n");
	printDecToHex(data.data(), data.size());

	printf("\tCHK :\n");
	printDecToHex(chk.data(), chk.size());
}
