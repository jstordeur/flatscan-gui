What is it :
------------

This repository provide a library to communicate with a Flatscan LZR in C++ and in a UNIX environnement.

Getting started :
-----------------

```c++

#include <vector>
#include <iostream>

#include "flatscan.hpp"

int main(void) {

    //Rect2 and Pol2 are provided with the library
    std::vector<Rect2> cloudPoints;
    
    //Create a new object from Flatscan Class
    Flatscan* fs = new Flatscan();
        fs->connect();  //Connect the comport
        fs->init();     //Init the Flatscan
    
    //Get measurments and transform them to rectangular coordonates
    fs->get_MeasurmentsRect(&cloudPoints);
    
    //Print the results
    for(auto it; it != cloudPoints.end(); ++it {
        std::cout << it->toString() << std::endl;
    }
    
    //Close the comport
    fs->close();
    
    //Delete de object and release memory
    delete fs;
    
    return 0;
}
```

How it works :
--------------