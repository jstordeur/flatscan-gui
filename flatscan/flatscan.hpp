#ifndef FLATSCAN_H
#define FLATSCAN_H

#include <stdio.h>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>

#include "const.hpp"
#include "command.hpp"
#include "frame.hpp"
#include "Utility/rect.hpp"
#include "Utility/pol.hpp"

//---------------------------------------------------------//
//--------------------------class--------------------------//
//---------------------------------------------------------//

class Frame;

class Flatscan{
private:
	int baudrate;
	int portcom;
	Config config;
	Frame *frame;
	unsigned int canNumber = 0;

	std::vector<octet> rawDistance; //coded on 2 bytes
	std::vector<octet> rawRemission; //coded on 2 bytes
	std::vector<int> distance; //coded on int
	std::vector<int> remission; //coded on int
	std::vector<Rect2> cloudPoints; //coded on Rect

	std::vector<octet> header;

	float angleStep;

    bool get_Measurments(bool reverse);

	//Extract header from data field of frame
	void extractHeader();

	//Interpret data field of frame in regards with DIST or REMI or DIST+REMI
	bool store_Measurments();
	//Transform measurments 2bytes into int or Rect.
    bool compute_Measurments(bool reverse);
	//Some variables need to be updated after set_parameters
	bool update_Config();
	bool update_CAN();

public:
	Flatscan();
	Flatscan(int portcom);
	Flatscan(int portcom, int baudrate);
	~Flatscan();

	bool connect();
	void close();
	void init();

	//Flatscan built-in commands
	bool get_Identity(Identity* id); ///Send null_ptr if you only want a canNumber update
	bool set_Baudrate(int baudrate);
	bool get_Parameters();
	bool set_Parameters(Config param_);
	bool get_Emergency();
	bool set_Led(LED led);

			//ToImplement
			bool store_Parameters();
			bool reset_MDI_Counter();
			bool reset_Heartbeat_Counter();
			bool reset_Emergency_Counter();

	//Enhanced commands
	bool get_ClosestObject(Measurement* m);
    bool get_MeasurmentsRect(std::vector<Rect2> *pts, bool reverse = false); ///Return measurment in rect coordonates

	//Setter's & Getter's
	int get_Portcom() {return this->portcom;};
	int get_CanNumber(bool needUpdate);
	float get_AngleStep() {return this->angleStep;};
	Config get_Config() {return this->config;};
	unsigned int get_CAN() {return this->canNumber;};

	//I love you <3
	friend class Command;
};

#endif
