#ifndef POL_H
#define POL_H

#include <stdio.h>
#include <math.h>

#include "rect.hpp"
#include "../const.hpp"

class Rect2;

class Pol2 {
	public :
		float angle;
		float rho;
		float radius;
		float direction;

		Pol2();
		Pol2(float angle, float rho, float direction = 0, float radius = 0);
		Pol2(Rect2 r);

		bool operator<(const Pol2& p);
		bool operator>(const Pol2& p);
		Pol2 operator+(const Pol2& p_);

		void toString() {
			printf("Pol2 (%f %f | %f)\n", rho, angle*TO_DEG, direction*TO_DEG);
		};

	private :
		Rect2 toRect(Pol2 p);
		Pol2 toPol(Rect2 r);
};

inline void writePol2VectorInFile(std::vector<Pol2> r, std::string dest) {
	std::ofstream myfile;
	myfile.open(dest);

	for(int i = 0; i < r.size(); i++) {
		myfile << r[i].rho << " " << r[i].angle * TO_DEG << std::endl;
	}

	myfile.close();
}

#endif
