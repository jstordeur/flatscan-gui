#include "pol.hpp"

Pol2::Pol2() {
	this->rho = 0;
	this->angle = 0;
	this->radius = 0;
	this->direction = 0;
}

Pol2::Pol2(float angle, float rho, float direction, float radius) {
	this->angle = angle;
	this->rho = rho;
	this->direction = direction;
	this->radius = radius;
}

Pol2::Pol2(Rect2 r) {
	*this = this->toPol(r);
}

bool Pol2::operator<(const Pol2& p){
	return rho < p.rho;
};

bool Pol2::operator>(const Pol2& p){
	return rho > p.rho;
};

Pol2 Pol2::operator+(const Pol2& p_) {
	Rect2 r_(p_);
	Rect2 this_(*this);

	return Pol2(r_ + this_);
}

Rect2 Pol2::toRect(Pol2 p) {
	return Rect2(p.rho * cos(p.angle), p.rho * sin(p.angle), p.direction);
};

Pol2 Pol2::toPol(Rect2 r) {
	Pol2 p = Pol2(0, 0);

	p.rho = sqrt(r.x * r.x + r.y * r.y);
	p.angle = atan2(r.y, r.x);
	p.direction = r.direction;

	if(p.angle < 0) p.angle += 2*M_PI;
	if(p.direction < 0) p.direction += 2*M_PI;

	return p;
}
