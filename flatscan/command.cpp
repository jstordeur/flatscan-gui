#include "command.hpp"

using namespace std;

int Command::port = 0;
int Command::header = 0;
int Command::buffer_Size = 0;
int Command::frameSize = 0;
octet Command::buffer[BUF_SIZE-1];

//Look at this. But try to not get an orgasm on it :3
bool Command::sendFrameReadAck(Flatscan* fs)
{
	header = 0;
	buffer_Size = 0;
	frameSize = 0;

	port = fs->get_Portcom();

	sendFrame(fs->frame);

	//wait to receive the whole frame //C'est un peu empirique c'te connerie
	usleep(57000);

	readBuffer();

	searchMyFrame(fs->frame->cmd.data());

	verifyMyFrame(fs->frame->chk.data());

	//Arithmetic of pointers occurs here
	fs->frame->set_Frame(buffer+header, frameSize);

	return false;
}

void Command::sendFrame(Frame *fr)
{
	//flush RX and TX buffer of port
	RS232_flushRXTX(port);

	//send to port
	if(RS232_SendBuf(port, fr->compute().data(), fr->compute().size()) == -1)
	{
		throw string("Could not send data to device\n");
	}
	if(MASTER_DEBUG && DEBUG_COMM)
	{
		printf("\n%s : Frame sent to device. (%d octets)\n", PREFIX_COMM, fr->compute().size());
		if(MASTER_DEBUG && DEBUG_HARD){printf("-->Frame sent :\n"); fr->toString();}
	}
}

void Command::readBuffer()
{
	//Read buffer from port
	buffer_Size = RS232_PollComport(port, buffer, BUF_SIZE-1);

	if(buffer_Size <= 0)
	{
		throw string("No data available\n");
	}
	if(MASTER_DEBUG && DEBUG_COMM)
	{
		printf("\n%s : Buffer read from device. (%d octets)\n", PREFIX_COMM, buffer_Size);
		if(MASTER_DEBUG && DEBUG_HARD){printf("-->Buffer read:\n"); printDecToBit(buffer, buffer_Size);}
	}
}

void Command::searchMyFrame(octet* ACK)
{
	//HARDCODED
	//ACK of Set_parameters is Send_parameters
	if(ACK[0] == 0x53 && ACK[1] == 0xC3) { ACK[0] = 0x54; }

	bool stopMe = false;
	do{
		if(MASTER_DEBUG && DEBUG_HARD && DEBUG_COMM)printf("\theader %d\n", header);
		//to avoid segmentations fault
		if(header + 12 < buffer_Size){
			//Search for synchronisation pattern
			if(buffer[header] == 0xBE && buffer[header+1] == 0xA0 && buffer[header+2] == 0x12 && buffer[header+3] == 0x34)
			{
				frameSize = buffer[header + 6] * 256 + buffer[header + 5];

				//Verify ACK
				if(buffer[header+11] == ACK[0] && buffer[header+12] == ACK[1]){
					stopMe = true;
					if(MASTER_DEBUG && DEBUG_COMM){
						printf("\nFrame found at %d with size of %d\n", header, frameSize);
						if(MASTER_DEBUG && DEBUG_HARD){printf("-->Frame :\n"); printDecToHex(buffer+header, frameSize);}
					}
				}
				//go to next message
				else{
					//to avoid segmentations fault
					if(header + frameSize < buffer_Size){
						header += frameSize;
					}
					else
						throw string("Frame not found in buffer\n");
				}
			}
			else
				header++;
		}
		else
			throw string("Frame not found in buffer\n");
	}while(!stopMe);
}

void Command::verifyMyFrame(octet* CRC16)
{
	//calculate crc16 from frame received
	word crc16 = Frame::CRC16(buffer+header, frameSize-2);

	//compare with crc16 received from frame
	if(buffer[header+frameSize-2] != crc16%256 || buffer[header+frameSize-1] != crc16/256){
		throw string("CRC16 no matched\n");
	}
	else
		if(MASTER_DEBUG && DEBUG_COMM){
			printf("\nCRC16 matched. Message integrity is assured. CRC16 :\n");
			if(MASTER_DEBUG && DEBUG_HARD){octet temp[2] = {crc16%256, crc16/256}; printDecToHex(temp, 2);
			}
		}
}
