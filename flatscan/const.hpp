#ifndef CONST_H
#define CONST_H

#include <bitset>
#include <iostream>
#include <math.h>
#include <fstream>
#include <vector>

//Lot's of defines
#define DEFAULT_PORTCOM 16
#define DEFAULT_BAUDRATE 921600

#define DEFAULT_DEV "/dev/i2c-1"

#define MASTER_DEBUG 0
#define DEBUG_COMM 0
#define DEBUG_FLAT 0
#define DEBUG_FRAME 0
#define DEBUG_HARD 0

#define PREFIX_COMM "[COMMAND]"
#define PREFIX_FLATSCAN "[FLATSCAN]"
#define PREFIX_FRAME "[FRAME]"


#define SIZE_SYNC 11
#define SIZE_CMD 2
#define SIZE_DATA 1609
#define SIZE_DATA_CAN_CNTR 6
#define SIZE_DATA_CTN 2
#define SIZE_DATA_FACET 1
#define SIZE_DATA_SPOT 2
#define SIZE_CHK 2
#define SIZE_FRAME_IF_DATA_NULL 15
#define BUF_SIZE 3251

#define BEA_POLYNOM 0x90d9


#define TO_DEG 57.295779513
#define TO_RAD 00.017453293

#define CLEAR std::cout << "\x1B[2J\x1B[H"

typedef unsigned short word;
typedef unsigned char octet;

//inline functions
inline void printDecToHex(octet *tab, int size) {
	printf("\n%d OCTETs to print.\n", size);
	for(int i = 0; i < size; i++)
	{
		printf("%02X ", tab[i]);
	}
	printf("\n");
}

inline void printDecToBit(octet* tab, int size) {
	printf("%d OCTETs to print.\n", size);
	for(int i = 0; i < size; i++)
	{
		std::bitset<8> x(tab[i]);
		std::cout << x << " ";
	}
	std::cout << std::endl;
}

//Enums
enum State {
	ENABLE = 0x01,
	DISABLE = 0x00
};

enum LED {
	IS_INIT,
	ERROR
};

enum MDI_Information {
	DISTANCE = 0x00,
	REMISSION = 0x01,
	DISTANCE_REMISSION = 0x02
};

enum Detection_Field_Mode {
	HS = 0x00,
	HD = 0x01
};

enum MeasurementMode {
	SINGLE_SHOT = 0x00,
	CONTINUOUS = 0x01
};

//Structs
struct Config {
	MeasurementMode Measurement_Mode;
	State CTN;
	MDI_Information MDI_Mode;
	Detection_Field_Mode Detection_Mode;
	int Sensitivity;
	int NbrSpots;
	int AngleFirst;
	int AngleLast;
	State CAN_FrameCntr;
	int HeartBeatPeriod;
	State Facet_Nbr;
	int Averaging;
};

struct Measurement {
	int dist;
	float remission;
	float angle;
};

struct Identity {
	unsigned int partNumber;
	int softwareVersion;
	int softwareRevision;
	int softwarePrototype;
	unsigned int canNumber;

	void toString() {
		printf("partNumber %d\n"
		"softwareVersion %d\n"
		"softwareRevision %d\n"
		"softwarePrototype %d\n"
		"canNumber %d\n",
		partNumber, softwareVersion, softwareRevision, softwarePrototype, canNumber);
	}
};

#endif
