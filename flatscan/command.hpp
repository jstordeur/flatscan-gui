#ifndef COMMAND_H
#define COMMAND_H

#include <vector>

#include "RS232/rs232.h"
#include "frame.hpp"
#include "flatscan.hpp"
#include "const.hpp"

class Flatscan;

class Command{
private:
	static int port;	//Because we need to know where to send the frame

	//Wow, I can hear the debugger shouting to segmentation error from here 3:{
	static octet buffer[BUF_SIZE-1];	//Well, the name is quite obvious --'
	static int buffer_Size;	//Please !
	static int header;	//Represent a "cursor" in the array buffer pointing toward the begining of frame
	static int frameSize;	//No, NO !!

	//NextGen
	static void sendFrame(Frame *fr); //send frame.
	static void readBuffer(); //read and write the statics members buffer and buffer_size.
	static void searchMyFrame(octet* ACK); //must be 2 in size. Search where the frame begin and write it into static member header.
	static void verifyMyFrame(octet* CRC16); //Verify the crc16.
	static void writeMyFrame(); //Improve Write something here. Don't be lazy --'

public:
	//Hell yeah, only one function with public access. This... This is amazing :3
	static bool sendFrameReadAck(Flatscan* fs);
};

#endif
