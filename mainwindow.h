#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <flatscan/flatscan.hpp>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void connect();
    void refresh();
    void flag();
    void update();

private:
    int tiimer = 0;
    Ui::MainWindow *ui;
    Flatscan* fs = new Flatscan();
    QTimer *timer;
    bool canUpdate = false;
    bool connected = false;
};

#endif // MAINWINDOW_H
